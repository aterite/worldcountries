//
//  CountryCell.m
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "CountryCell.h"
#import "Settings.h"
#import <UIImageView+WebCache.h>

@interface CountryCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;

@property (nonatomic, strong) UIImage *flagPlaceholderImage;

@end

@implementation CountryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.flagPlaceholderImage = [UIImage imageNamed:WCFlagSmallPlaceholderImageName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCountry:(Country *)country {
    _country = country;
    
    self.nameLabel.text = country.name;
    [self.flagImageView sd_setImageWithURL:[country smallImageURL] placeholderImage:self.flagPlaceholderImage];
}

@end
