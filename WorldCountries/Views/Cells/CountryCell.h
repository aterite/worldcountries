//
//  CountryCell.h
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Country+CustomMethods.h"

@interface CountryCell : UITableViewCell

@property (nonatomic, strong) Country *country;

@end
