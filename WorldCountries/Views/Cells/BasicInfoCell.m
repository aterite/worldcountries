//
//  BasicInfoCell.m
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "BasicInfoCell.h"
#import <UIImageView+WebCache.h>
#import "Settings.h"
#import "WorldCountries+CoreDataModel.h"


@interface BasicInfoCell ()

@property (nonatomic, strong) UIImage *placeholderImage;

@property (nonatomic, weak) IBOutlet UIImageView *flagImageView;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblCapital;
@property (nonatomic, weak) IBOutlet UILabel *lblArea;
@property (nonatomic, weak) IBOutlet UILabel *lblPopulation;
@property (nonatomic, weak) IBOutlet UILabel *lblDenonym;
@property (nonatomic, weak) IBOutlet UILabel *lblGini;
@property (nonatomic, weak) IBOutlet UILabel *lblCallingCodes;
@property (nonatomic, weak) IBOutlet UILabel *lblDomain;


@end

@implementation BasicInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.placeholderImage = [UIImage imageNamed:WCFlagSmallPlaceholderImageName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCountry:(Country *)country {
    _country = country;
    
    [self.flagImageView sd_setImageWithURL:country.bigImageURL placeholderImage:self.placeholderImage];
    
    NSString *countryName = country.name;
    
    if (![countryName isEqualToString:country.nativeName]) {
        countryName = [countryName stringByAppendingFormat:@" (%@)", country.nativeName];
    }
    
    self.lblName.text = countryName;

    self.lblCapital.text = [country.capital length] ? country.capital : NSLocalizedString(@"COUNTRY_NO_CAPITAL", @"");
    
    self.lblArea.text = country.area ? [NSString stringWithFormat:NSLocalizedString(@"COUNTRY_AREA_FORMAT", @""), country.area] : NSLocalizedString(@"COUNTRY_NO_AREA", @"");
    
    self.lblPopulation.text = country.population ? country.population.stringValue : NSLocalizedString(@"COUNTRY_NO_POPULATION", @"");
    
    self.lblDenonym.text = [country.denonym length] ? country.denonym : NSLocalizedString(@"COUNTRY_NO_DENONYM", @"");
    
    self.lblGini.text = country.gini ? country.gini.stringValue : NSLocalizedString(@"COUNTRY_NO_GINI", @"");
    
    NSMutableArray *callingCodeValues = [NSMutableArray new];
    
    for (CallingCode *callingCode in country.callingCodes) {
        [callingCodeValues addObject:callingCode.value];
    }
    
    NSString *callingCodes = [callingCodeValues componentsJoinedByString:@", "];
    
    self.lblCallingCodes.text = [callingCodes length] ? callingCodes : NSLocalizedString(@"COUNTRY_NO_CALLING_CODE", @"");
    
    NSMutableArray *domainValues = [NSMutableArray new];
    
    for (Domain *domain in country.domains) {
        [domainValues addObject:domain.value];
    }
    
    NSString *domains = [domainValues componentsJoinedByString:@", "];
    
    self.lblDomain.text = [domains length] ? domains: NSLocalizedString(@"COUNTRY_NO_DOMAINS", @"");
}

@end
