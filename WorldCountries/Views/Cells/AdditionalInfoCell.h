//
//  AdditionalInfoCell.h
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdditionalInfoCell : UITableViewCell

- (void)setTitle:(NSString *)title andDetail:(NSString *)detail;

@end
