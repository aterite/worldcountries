//
//  AdditionalInfoCell.m
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "AdditionalInfoCell.h"

@interface AdditionalInfoCell ()

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblDetail;

@end

@implementation AdditionalInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title andDetail:(NSString *)detail {
    self.lblTitle.text = title;
    self.lblDetail.text = detail;
}

@end
