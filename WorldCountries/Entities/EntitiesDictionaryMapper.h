//
//  EntitiesMap.h
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorldCountries+CoreDataModel.h"
#import "NSManagedObject+CustomMethods.h"

@interface EntitiesDictionaryMapper : NSObject

- (void)mapCountriesArray:(NSArray<NSDictionary *> * _Nonnull)countriesArray intoContext:(NSManagedObjectContext * _Nonnull)context;

@end
