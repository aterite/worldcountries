//
//  NSManagedObject+CustomMethods.m
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "NSManagedObject+CustomMethods.h"

@implementation NSManagedObject (CustomMethods)

- (void)wc_setValuesFromDictionary:(NSDictionary *)dictionary customMapping: (NSDictionary *)mappingDictionary {
    
    NSDictionary *attributes = [[self entity] attributesByName];
    
    for (NSAttributeDescription *attribute in [self entity]) {
        id value;
        if (mappingDictionary) {
            NSString *customKey = mappingDictionary[attribute.name];
            value = [dictionary objectForKey:customKey];
        } else {
            value = [dictionary objectForKey:attribute.name];
        }
        
        if (value == nil || [value isEqual:[NSNull null]]) {
            // Don't set if nil or NSNull
            continue;
        }
        
        NSAttributeType attributeType = [[attributes objectForKey:attribute.name] attributeType];
        
        if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]])) {
            value = [value stringValue];
        } else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]])) {
            value = [NSNumber numberWithInteger:[value  longValue]];
        } else if ((attributeType == NSFloatAttributeType) && ([value isKindOfClass:[NSString class]])) {
            value = [NSNumber numberWithDouble:[value doubleValue]];
        }
        
        [self setValue:value forKey:attribute.name];
    }
}

- (void)wc_setValuesFromDictionary:(NSDictionary *)dictionary {
    [self wc_setValuesFromDictionary:dictionary customMapping:nil];
}

@end
