//
//  Country+CustomMethods.m
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "Country+CustomMethods.h"
#import "Settings.h"

@implementation Country (CustomMethods)

- (NSURL *)imageURLWithFormat:(NSString *)format {
    NSString *code = self.alpha2Code.lowercaseString;
    
    if (!code) {
        return nil;
    }
    
    NSString *urlString = [NSString stringWithFormat:format, code];
    return [NSURL URLWithString:urlString];
}

- (NSURL *)smallImageURL {
    return [self imageURLWithFormat:WCFlagSmallUrlStringFormat];
}

- (NSURL *)bigImageURL {
    return [self imageURLWithFormat:WCFlagBigUrlStringFormat];
}

@end
