//
//  NSManagedObject+CustomMethods.h
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (CustomMethods)

// custom initialization of properties
- (void)wc_setValuesFromDictionary: (NSDictionary * _Nonnull)dictionary;
- (void)wc_setValuesFromDictionary:(NSDictionary *_Nonnull)dictionary customMapping: (NSDictionary *_Nullable)mappingDictionary;

@end
