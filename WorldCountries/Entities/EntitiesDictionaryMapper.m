//
//  EntitiesMap.m
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "EntitiesDictionaryMapper.h"

@interface EntitiesDictionaryMapper ()

@property (nonatomic, strong) NSManagedObjectContext *context;

@property (nonatomic, strong) NSMutableArray<Country *> *currentCountries;
@property (nonatomic, strong) NSMutableArray<NSArray<NSString *> *> *bordersArray;
@property (nonatomic, strong) NSMutableArray<Currency *> *currentCurrencies;
@property (nonatomic, strong) NSMutableArray<Language *> *currentLanguages;
@property (nonatomic, strong) NSMutableArray<Region *> *currentRegions;
@property (nonatomic, strong) NSMutableArray<RegionalBloc *> *currentRegionalBlocks;
@property (nonatomic, strong) NSMutableArray<Subregion *> *currentSubregions;
@property (nonatomic, strong) NSMutableArray<Timezone *> *currentTimezones;

@end

@implementation EntitiesDictionaryMapper

- (void)mapCountriesArray:(NSArray<NSDictionary *> * _Nonnull)countriesArray intoContext:(NSManagedObjectContext *)context {
    
    self.currentCountries = [NSMutableArray new];
    self.bordersArray = [NSMutableArray new];
    self.currentCurrencies = [NSMutableArray new];
    self.currentLanguages = [NSMutableArray new];
    self.currentRegions = [NSMutableArray new];
    self.currentRegionalBlocks = [NSMutableArray new];
    self.currentSubregions = [NSMutableArray new];
    self.currentTimezones = [NSMutableArray new];
    
    self.context = context;
    
    for (NSDictionary *dict in countriesArray) {
        [self countryFromDictionary:dict];
    }
    
    [self setupBorderRelationships];
}

- (Country *)fetchCountryWithAlpha3Code: (NSString *)alpha3Code {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alpha3Code == %@", alpha3Code];
    
    
    
    
    NSArray *countries = [self.currentCountries filteredArrayUsingPredicate:predicate];
    
    return [countries firstObject];
}

- (NSSet<AlternativeSpelling *> * _Nullable)alternativeSpellingsFromArray: (NSArray *_Nonnull)sourceArray {
    NSMutableSet<AlternativeSpelling *> *allValues = [NSMutableSet set];
    
    for (NSString *stringValue in sourceArray) {
        AlternativeSpelling *element = [[AlternativeSpelling alloc] initWithContext:self.context];
        element.value = stringValue;
        [allValues addObject:element];
    }
    
    return allValues;
}

- (NSSet<BlocAcronym *> * _Nullable)blockAcronymsFromArray: (NSArray *_Nonnull)sourceArray {
    NSMutableSet<BlocAcronym *> *allValues = [NSMutableSet set];
    
    for (NSString *stringValue in sourceArray) {
        BlocAcronym *element = [[BlocAcronym alloc] initWithContext:self.context];
        element.value = stringValue;
        [allValues addObject:element];
    }
    
    return allValues;
}

- (NSSet<BlocName *> * _Nullable)blockNamesFromArray: (NSArray *_Nonnull)sourceArray {
    NSMutableSet<BlocName *> *allValues = [NSMutableSet set];
    
    for (NSString *stringValue in sourceArray) {
        BlocName *element = [[BlocName alloc] initWithContext:self.context];
        element.value = stringValue;
        [allValues addObject:element];
    }
    
    return allValues;
}

- (NSSet<CallingCode *> * _Nullable)callingCodesFromArray: (NSArray *_Nonnull)sourceArray {
    NSMutableSet<CallingCode *> *allValues = [NSMutableSet set];
    
    for (NSString *stringValue in sourceArray) {
        CallingCode *element = [[CallingCode alloc] initWithContext:self.context];
        element.value = stringValue;
        [allValues addObject:element];
    }
    
    return allValues;
}

- (Coordinates * _Nullable)coordinatesFromArray: (NSArray *_Nonnull)sourceArray {
    
    if (sourceArray.count != 2) {
        return nil;
    }
    
    Coordinates *coordinates = [[Coordinates alloc] initWithContext:self.context];
    
    coordinates.latitude = sourceArray[0];
    coordinates.longitude = sourceArray[1];
    
    return coordinates;
}

- (Country * _Nullable)countryFromDictionary: (NSDictionary *_Nonnull)sourceDictionary {
    NSDictionary *mappingDictionary = @{
                                        @"alpha2Code" : @"alpha2Code",
                                        @"alpha3Code" : @"alpha3Code",
                                        @"area" : @"area",
                                        @"capital" : @"capital",
                                        @"denonym" : @"demonym",
                                        @"gini" : @"gini",
                                        @"name" : @"name",
                                        @"nativeName" : @"nativeName",
                                        @"numericCode" : @"numericCode",
                                        @"population" : @"population"
                                        };
    
    Country *newElement = [[Country alloc] initWithContext:self.context];
    
    [newElement wc_setValuesFromDictionary:sourceDictionary customMapping:mappingDictionary];
    
    NSArray *alternativeSpellingArray = sourceDictionary[@"altSpellings"];
    if ([alternativeSpellingArray count]) {
        NSSet *spellings = [self alternativeSpellingsFromArray:alternativeSpellingArray];
        
        [newElement setAlternativeSpellings:spellings];
    }
    
    NSArray *borderingCountries = sourceDictionary[@"borders"];
    
    [self.bordersArray addObject:borderingCountries];
    
    NSArray *callingCodes = sourceDictionary[@"callingCodes"];
    if ([callingCodes count]) {
        [newElement setCallingCodes:[self callingCodesFromArray:callingCodes]];
    }
    
    NSArray *coordinates = sourceDictionary[@"latlng"];
    if ([coordinates count]) {
        [newElement setCoordinates:[self coordinatesFromArray:coordinates]];
    }
    
    NSArray *currencies = sourceDictionary[@"currencies"];
    for (NSDictionary *currencyDict in currencies) {
        Currency *currency = [self currencyFromDictionary:currencyDict];
        if (currency) {
            [newElement addCurrenciesObject:currency];
        }
    }
    
    NSArray *domainsArray = sourceDictionary[@"topLevelDomain"];
    if ([domainsArray count]) {
        NSSet<Domain *> *domains = [self domainsFromArray:domainsArray];
        [newElement setDomains:domains];
    }
    
    NSArray *languagesArray = sourceDictionary[@"languages"];
    for (NSDictionary *dict in languagesArray) {
        Language *language = [self languageFromDictionary:dict];
        [newElement addLanguagesObject:language];
    }
    
    NSString *regionName = sourceDictionary[@"region"];
    if ([regionName length]) {
        Region *region = [self regionFromString:regionName];
        [newElement setRegion:region];
    }
    
    NSString *subregionName = sourceDictionary[@"subregion"];
    if ([subregionName length]) {
        Subregion *subregion = [self subregionFromString:subregionName];
        [newElement setSubregion:subregion];
    }
    
    NSArray *regionalBlocsArray = sourceDictionary[@"regionalBlocs"];
    for (NSDictionary *dictionary in regionalBlocsArray) {
        RegionalBloc *bloc = [self regionalBlocFromDictionary:dictionary];
        [newElement addRegionalBlocsObject:bloc];
    }
    
    NSArray *timezonesArray = sourceDictionary[@"timezones"];
    for (NSString *timezoneString in timezonesArray) {
        Timezone *timezone = [self timezoneFromString:timezoneString];
        [newElement addTimezonesObject:timezone];
    }
    
    NSDictionary *translationsDictionary = sourceDictionary[@"translations"];
    
    Translation *translation = [self translationFromDictionary:translationsDictionary];
    [newElement setTranslation:translation];
    
    [self.currentCountries addObject:newElement];
    
    return newElement;
}

- (void)setupBorderRelationships {
    for (int i = 0; i < self.currentCountries.count; i++) {
        Country *country = self.currentCountries[i];
        NSArray<NSString *> *borders = self.bordersArray[i];
        
        for (NSString *alpha3Code in borders) {
            Country *borderedCountry = [self fetchCountryWithAlpha3Code:alpha3Code];
            
            if (borderedCountry) {
                [country addBorderedCountriesObject:borderedCountry];
            }
        }
        
    }
}

- (Currency *_Nullable)currencyFromDictionary: (NSDictionary *_Nonnull)sourceDictionary {
    
    for (NSString *value in sourceDictionary.allValues) {
        if ([value isEqual:[NSNull null]]) {
            return nil;
        }
    }
    
    NSDictionary *mappingDictionary = @{
                                        @"code" : @"code",
                                        @"name" : @"name",
                                        @"symbol" : @"symbol"
                                        };
    
    NSString *name = sourceDictionary[@"name"];
    NSString *code = sourceDictionary[@"code"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ AND code == %@", name, code];
    
    NSArray *currentElements = [self.currentCurrencies filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        Currency *newElement = [[Currency alloc] initWithContext:self.context];
        [newElement wc_setValuesFromDictionary:sourceDictionary customMapping:mappingDictionary];
        
        [self.currentCurrencies addObject:newElement];
        
        return newElement;
    }
}

- (NSSet<Domain *> * _Nullable)domainsFromArray: (NSArray *_Nonnull)sourceArray {
    NSMutableSet<Domain *> *allValues = [NSMutableSet set];
    
    for (NSString *stringValue in sourceArray) {
        Domain *element = [[Domain alloc] initWithContext:self.context];
        element.value = stringValue;
        [allValues addObject:element];
    }
    
    return allValues;
}

- (Language *_Nonnull)languageFromDictionary: (NSDictionary *_Nonnull)sourceDictionary {
    
    NSDictionary *mappingDictionary = @{
                                        @"iso6391" : @"iso69_1",
                                        @"iso6392" : @"iso639_2",
                                        @"name" : @"name",
                                        @"nativeName" : @"nativeName"
                                        };
    
    NSString *name = sourceDictionary[@"name"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    
    NSArray *currentElements = [self.currentLanguages filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        Language *newElement = [[Language alloc] initWithContext:self.context];
        [newElement wc_setValuesFromDictionary:sourceDictionary customMapping:mappingDictionary];
        
        [self.currentLanguages addObject:newElement];
        
        return newElement;
    }
}

- (Region *_Nonnull)regionFromString: (NSString *_Nonnull)sourceString {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", sourceString];
    
    NSArray *currentElements = [self.currentRegions filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        Region *newElement = [[Region alloc] initWithContext:self.context];
        newElement.name = sourceString;
        
        [self.currentRegions addObject:newElement];
        
        return newElement;
    }
}

- (RegionalBloc *_Nonnull)regionalBlocFromDictionary: (NSDictionary *_Nonnull)sourceDictionary {
    
    NSDictionary *mappingDictionary = @{
                                        @"name" : @"name",
                                        @"acronym" : @"acronym"
                                        };
    
    NSString *name = sourceDictionary[@"name"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    
    NSArray *currentElements = [self.currentRegionalBlocks filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        RegionalBloc *newElement = [[RegionalBloc alloc] initWithContext:self.context];
        [newElement wc_setValuesFromDictionary:sourceDictionary customMapping:mappingDictionary];
        
        NSArray *otherAcronyms = sourceDictionary[@"otherAcronyms"];
        if ([otherAcronyms count]) {
            NSSet<BlocAcronym *> *acronyms = [self blockAcronymsFromArray:otherAcronyms];
            [newElement addOtherAcronyms:acronyms];
        }
        
        NSArray *otherNames = sourceDictionary[@"otherNames"];
        if ([otherNames count]) {
            NSSet<BlocName *> *names = [self blockNamesFromArray:otherNames];
            [newElement addOtherNames:names];
        }
        
        [self.currentRegionalBlocks addObject:newElement];
        
        return newElement;
    }
}


- (Subregion *_Nonnull)subregionFromString: (NSString *_Nonnull)sourceString {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", sourceString];
    
    NSArray *currentElements = [self.currentSubregions filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        Subregion *newElement = [[Subregion alloc] initWithContext:self.context];
        newElement.name = sourceString;
        
        [self.currentSubregions addObject:newElement];
        
        return newElement;
    }
}

- (Timezone *_Nonnull)timezoneFromString: (NSString *_Nonnull)sourceString {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"value == %@", sourceString];
    
    NSArray *currentElements = [self.currentTimezones filteredArrayUsingPredicate:predicate];
    
    if (currentElements.count > 0) {
        return currentElements.firstObject;
    } else {
        Timezone *newElement = [[Timezone alloc] initWithContext:self.context];
        newElement.value = sourceString;
        
        [self.currentTimezones addObject:newElement];
        
        return newElement;
    }
}

- (Translation *_Nonnull)translationFromDictionary: (NSDictionary *_Nonnull)sourceDictionary {
    Translation *translation = [[Translation alloc] initWithContext:self.context];
    
    NSDictionary *mappingDictionary = @{
                                        @"brPortuguese" : @"br",
                                        @"french" : @"fr",
                                        @"german": @"de",
                                        @"italian" : @"it",
                                        @"japaneese" : @"ja",
                                        @"portuguese" : @"pt",
                                        @"spanish" : @"es"
                                        };
    
    [translation wc_setValuesFromDictionary:sourceDictionary customMapping:mappingDictionary];
    
    return translation;
}

@end
