//
//  Country+CustomMethods.h
//  WorldCountries
//
//  Created by armat on 5/26/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "Country+CoreDataClass.h"

@interface Country (CustomMethods)

- (NSURL * _Nullable)smallImageURL;
- (NSURL * _Nullable)bigImageURL;

@end
