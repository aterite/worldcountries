//
//  AppDelegate.h
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DatabaseService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

