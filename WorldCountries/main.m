//
//  main.m
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
