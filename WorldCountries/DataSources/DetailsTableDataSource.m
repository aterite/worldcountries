//
//  DetailsTableDataSource.m
//  WorldCountries
//
//  Created by armat on 5/27/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "DetailsTableDataSource.h"
#import "BasicInfoCell.h"
#import "AdditionalInfoCell.h"

@interface DetailsTableDataSource ()

@property (nonatomic, strong) Country *detailItem;

@end

@implementation DetailsTableDataSource

- (instancetype)initWithCountry:(Country *)country {
    self = [super init];
    
    if (self) {
        self.detailItem = country;
    }
    
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *title;
    NSString *detail;
    
    switch (indexPath.row) {
        case 0: {
            BasicInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([BasicInfoCell class])];
            cell.country = self.detailItem;
            
            return cell;
        }
        case 1: {
            // languages
            
            NSMutableArray<NSString *> *languageNames = [NSMutableArray new];
            
            for (Language *language in self.detailItem.languages) {
                [languageNames addObject:language.name];
            }
            
            NSString *languagesString = [languageNames componentsJoinedByString:@", "];
            
            if (![languagesString length]) {
                languagesString = NSLocalizedString(@"COUNTRY_NO_LANGUAGE", @"");
            }
            
            title = NSLocalizedString(@"LANGUAGES", @"");
            detail = languagesString;
            
            break;
        }
        case 2: {
            // region
            
            title = NSLocalizedString(@"REGION", @"");
            detail = [self.detailItem.region.name length] ? self.detailItem.region.name : NSLocalizedString(@"COUNTRY_NO_REGION", @"");
            break;
        }
        case 3:
            // subregion
            
            title = NSLocalizedString(@"SUBREGION", @"");
            detail = [self.detailItem.subregion.name length] ? self.detailItem.subregion.name : NSLocalizedString(@"COUNTRY_NO_SUBREGION", @"");
            break;
        case 4: {
            //borders
            
            NSMutableArray<NSString *> *borderedCountryNames = [NSMutableArray new];
            
            for (Country *country in self.detailItem.borderedCountries) {
                [borderedCountryNames addObject:country.name];
            }
            
            NSString *borderedCountriesString = [borderedCountryNames componentsJoinedByString:@", "];
            
            if(![borderedCountriesString length]) {
                borderedCountriesString = NSLocalizedString(@"COUNTRY_NO_BORDERED_COUNTRIES", @"");
            }
            
            title = NSLocalizedString(@"BORDERED_COUNTRIES", @"");
            detail = borderedCountriesString;
            
            break;
        }
        case 5: {
            //timezones
            
            NSMutableArray<NSString *> *timezoneValues = [NSMutableArray new];
            
            for (Timezone *timezone in self.detailItem.timezones) {
                [timezoneValues addObject:timezone.value];
            }
            
            NSString *timezonesString = [timezoneValues componentsJoinedByString:@", "];
            
            if (![timezonesString length]) {
                timezonesString = NSLocalizedString(@"COUNTRY_NO_TIMEZONES", @"");
            }
            
            title = NSLocalizedString(@"TIMEZONES", @"");
            detail = timezonesString;
            
            break;
        }
            
        case 6: {
            // currencies
            NSMutableArray<NSString *> *currencyNames = [NSMutableArray new];
            
            for (Currency *currency in self.detailItem.currencies) {
                NSString *currencyDescription = [NSString stringWithFormat:@"%@ (%@)", currency.name, currency.symbol];
                [currencyNames addObject:currencyDescription];
            }
            
            NSString *currenciesString = [currencyNames componentsJoinedByString:@", "];
            
            if (![currenciesString length]) {
                currenciesString = NSLocalizedString(@"COUNTRY_NO_CURRENCY", @"");
            }
            
            title = NSLocalizedString(@"CURRENCY", @"");
            detail = currenciesString;
            
            break;
        }
        case 7: {
            // regional blocs
            
            NSMutableArray<NSString *> *blocNames = [NSMutableArray new];
            
            for (RegionalBloc *bloc in self.detailItem.regionalBlocs) {
                [blocNames addObject:bloc.name];
            }
            
            NSString *blocsString = [blocNames componentsJoinedByString:@"\n"];
            
            if (![blocsString length]) {
                blocsString = NSLocalizedString(@"COUNTRY_NO_BLOC", @"");
            }
            
            title = NSLocalizedString(@"BLOCS", @"");
            detail = blocsString;
            
            break;
        }
        case 8: {
            // alternative spellings
            
            NSMutableArray<NSString *> *alternativeSpellings = [NSMutableArray new];
            
            for (AlternativeSpelling *spelling in self.detailItem.alternativeSpellings) {
                [alternativeSpellings addObject:spelling.value];
            }
            
            NSString *spellingsString = [alternativeSpellings componentsJoinedByString:@"\n"];
            
            if (![spellingsString length]) {
                spellingsString = NSLocalizedString(@"COUNTRY_NO_ALTERNATIVE_SPELLINGS", @"");
            }
            
            title = NSLocalizedString(@"ALTERNATIVE_SPELLINGS", @"");
            detail = spellingsString;
            
            break;
        }
        case 9: {
            // translations
            
            Translation *translation = self.detailItem.translation;
            
            NSMutableArray<NSString *> *translations = [NSMutableArray new];
            
            [self addLanguage:NSLocalizedString(@"GERMAN", @"") andValue:translation.german toArray:translations];
            [self addLanguage:NSLocalizedString(@"SPANISH", @"") andValue:translation.spanish toArray:translations];
            [self addLanguage:NSLocalizedString(@"FRENCH", @"") andValue:translation.french toArray:translations];
            [self addLanguage:NSLocalizedString(@"JAPANESE", @"") andValue:translation.japanese toArray:translations];
            [self addLanguage:NSLocalizedString(@"ITALIAN", @"") andValue:translation.italian toArray:translations];
            [self addLanguage:NSLocalizedString(@"BRAZIL_PORTUGUESE", @"") andValue:translation.brPortuguese toArray:translations];
            [self addLanguage:NSLocalizedString(@"PORTUGUESE", @"") andValue:translation.portuguese toArray:translations];
            
            
            NSString *translationsString = [translations componentsJoinedByString:@"\n"];
            
            title = NSLocalizedString(@"TRANSLATIONS", @"");
            detail = translationsString;
            
            break;
        }
    }
    
    AdditionalInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AdditionalInfoCell class])];
    
    [cell setTitle:title andDetail:detail];
    
    return cell;
}

- (void)addLanguage:(NSString *)language andValue:(NSString *)value toArray:(NSMutableArray *)array {
    if (value) {
        NSString *translationLanguage = language;
        
        NSString *thisTranslation = [NSString stringWithFormat:@"%@: %@", translationLanguage, value];
        
        [array addObject:thisTranslation];
    }
}

@end
