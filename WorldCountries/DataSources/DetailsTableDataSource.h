//
//  DetailsTableDataSource.h
//  WorldCountries
//
//  Created by armat on 5/27/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WorldCountries+CoreDataModel.h"


@interface DetailsTableDataSource : NSObject <UITableViewDataSource>

- (instancetype _Nullable ) init __attribute__((unavailable("Use initWithCountry: instead.")));

- (instancetype _Nullable )initWithCountry:(Country *_Nonnull)country;

@end
