//
//  NetworkService.h
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkService : NSObject

- (instancetype _Nullable ) init __attribute__((unavailable("Use initWithURL: instead.")));

- (instancetype _Nullable )initWithURL: (NSURL *_Nonnull) requestURL;

- (void)retrieveAllCountries:(void (^_Nonnull)(NSArray<NSDictionary *> *_Nullable allCountries, NSError * _Nullable error))block;

@end
