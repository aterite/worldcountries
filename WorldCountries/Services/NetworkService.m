//
//  NetworkService.m
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "NetworkService.h"
#import <UIKit/UIKit.h>

@interface NetworkService ()

@property (nonatomic, copy) NSURL *requestURL;

@end

@implementation NetworkService


- (instancetype)initWithURL: (NSURL *) requestURL;
{
    self = [super init];
    if (self) {
        _requestURL = requestURL;
    }
    return self;
}

- (void)retrieveAllCountries:(void (^)(NSArray<NSDictionary *> * _Nullable allCountries, NSError * _Nullable error))block {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:self.requestURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (error == nil) {
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSError *error;
                
                NSArray<NSDictionary *> *allCountries = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error == nil) {
                        block(allCountries, nil);
                    } else {
                        block(nil, error);
                    }
                });
                
            });
        } else {
            block(nil, error);
        }
        
    }];
    
    [dataTask resume];
}

@end
