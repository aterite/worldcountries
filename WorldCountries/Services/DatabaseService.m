//
//  DatabaseService.m
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "DatabaseService.h"
#import "Settings.h"
#import "WorldCountries+CoreDataModel.h"
#import "NSManagedObject+CustomMethods.h"
#import "EntitiesDictionaryMapper.h"

@interface DatabaseService ()

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectContext *bgManagedObjectContext;

@end

@implementation DatabaseService

- (instancetype)initWithCompletionBlock:(void (^)())block {
    self = [super init];
    
    if (!self) return nil;
    
    [self setupCoreDataWithCompletionBlock:block];
    
    return self;
}

- (instancetype _Nullable ) initWithCountriesArray: (NSArray<NSDictionary *> *_Nonnull) countriesArray andCompletionBlock: (void (^_Nullable)())block {
    self = [super init];
    
    if (!self) return nil;
    
    // destroy old database
    [self destroyDatabaseWithCompletionBlock:^(BOOL success, NSError *error) {
        if (success) {
            // create new database with new received data
            [self setupCoreDataWithCompletionBlock:^{
                
                // map data in background thread with background context
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    self.bgManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
                    
                    self.bgManagedObjectContext.persistentStoreCoordinator = self.managedObjectContext.persistentStoreCoordinator;
                    
                    [self addCountriesFromArray: countriesArray];
                    
                    dispatch_async(dispatch_get_main_queue(), block);
                });
            }];
        } else {
            [self setupCoreDataWithCompletionBlock:block];
        }
    }];
    
    return self;
}

#pragma mark - Parsing

- (void)addCountriesFromArray: (NSArray<NSDictionary *> *)countriesArray {
    
    EntitiesDictionaryMapper *mapper = [EntitiesDictionaryMapper new];
    
    [mapper mapCountriesArray:countriesArray intoContext:self.bgManagedObjectContext];
    
    [self saveContext:self.bgManagedObjectContext];
}

#pragma mark - Core Data stack

- (NSURL *)storeURL {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    // The directory the application uses to store the Core Data store file.
    return [documentsURL URLByAppendingPathComponent:WCDatabaseStorageName];
}

- (NSPersistentStoreCoordinator *)newPersistentStoreCoordinator {
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:WCDatabaseModelName withExtension:@"momd"];
    
    NSAssert(modelURL, @"Failed to locate momd bundle in application");
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(mom, @"Failed to initialize mom from URL: %@", modelURL);
    
    return [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
}



- (void)setupCoreDataWithCompletionBlock: (void (^)())block {
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator = [self newPersistentStoreCoordinator];
    
    NSURL *storeURL = [self storeURL];
    
    NSError *error = nil;
    NSPersistentStore *store = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    
    NSAssert(store, @"Failed to initialize persistent store: %@\n%@", [error localizedDescription], [error userInfo]);
    
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [moc setPersistentStoreCoordinator:persistentStoreCoordinator];
    
    self.managedObjectContext = moc;
    
    if (!block) {
        //If there is no callback block we can safely return
        return;
    }
    
    block();
}

- (void)saveContext:(NSManagedObjectContext *)context {
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

- (void)destroyDatabaseWithCompletionBlock: (void (^)(BOOL success, NSError * error))block {
    
    NSPersistentStoreCoordinator *coordinator = [self newPersistentStoreCoordinator];
    NSURL *storeURL = [self storeURL];
    
    NSError *error;
    
    [coordinator destroyPersistentStoreAtURL:storeURL withType:NSSQLiteStoreType options:nil error:&error];
    
    block(error == nil, error);
}

@end
