//
//  DatabaseService.h
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DatabaseService : NSObject

- (instancetype _Nullable ) init __attribute__((unavailable("Use initWithCompletionBlock: instead.")));

- (instancetype _Nullable )initWithCompletionBlock: (void (^_Nullable)())block;

- (instancetype _Nullable ) initWithCountriesArray: (NSArray<NSDictionary *> *_Nonnull) countriesArray andCompletionBlock: (void (^_Nullable)())block;

@property (nonatomic, readonly) NSManagedObjectContext * _Nullable managedObjectContext;

@end
