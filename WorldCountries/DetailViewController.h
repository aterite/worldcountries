//
//  DetailViewController.h
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorldCountries+CoreDataModel.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Country *detailItem;

@end

