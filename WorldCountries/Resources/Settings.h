//
//  Settings.h
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const WCAllCountriesUrlString;
extern NSString * const WCFlagSmallUrlStringFormat;
extern NSString * const WCFlagBigUrlStringFormat;

extern NSString * const WCFlagSmallPlaceholderImageName;

extern NSString * const WCDatabaseModelName;
extern NSString * const WCDatabaseStorageName;
