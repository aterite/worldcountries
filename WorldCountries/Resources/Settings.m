//
//  Settings.m
//  WorldCountries
//
//  Created by armat on 5/25/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "Settings.h"

NSString * const WCAllCountriesUrlString = @"https://restcountries.eu/rest/v2/all";
NSString * const WCFlagSmallUrlStringFormat = @"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png250px/%@.png";

NSString * const WCFlagBigUrlStringFormat = @"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png1000px/%@.png";

NSString * const WCFlagSmallPlaceholderImageName = @"flagPlaceholder";

NSString * const WCDatabaseModelName = @"WorldCountries";
NSString * const WCDatabaseStorageName = @"WorldCountries.sqlite";
