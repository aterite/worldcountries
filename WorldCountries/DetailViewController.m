//
//  DetailViewController.m
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailsTableDataSource.h"

@interface DetailViewController () <UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) DetailsTableDataSource *dataSource;

@end

@implementation DetailViewController

- (void)configureView {
    // Update the user interface for the detail item.
    self.tableView.dataSource = self.dataSource;
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView.estimatedRowHeight = 400;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self configureView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Managing the detail item

- (void)setDetailItem:(Country *)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        
        self.dataSource = [[DetailsTableDataSource alloc] initWithCountry:newDetailItem];
        
        [self configureView];
    }
}

@end
