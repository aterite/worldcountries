//
//  MasterViewController.m
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "NetworkService.h"
#import "Settings.h"
#import "CountryCell.h"

@interface MasterViewController () <UISearchResultsUpdating>

@property (strong, nonatomic) NSFetchedResultsController<Country *> *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation MasterViewController

@synthesize managedObjectContext = _managedObjectContext;  //Must do this

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl beginRefreshing];
    
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    
    self.tableView.tableFooterView = [[UIView alloc] init];

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [self setupData];
}


- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupData {
    NSURL *url = [NSURL URLWithString:WCAllCountriesUrlString];
    
    NetworkService *networkService = [[NetworkService alloc] initWithURL:url];
    
    [networkService retrieveAllCountries:^(NSArray<NSDictionary *> * _Nullable allCountries, NSError * _Nullable error) {
        
        // we need to use databaseService in completion block
        __block DatabaseService *databaseService = [DatabaseService new];
        
        void (^completionBlock)() = ^{
            self.managedObjectContext = databaseService.managedObjectContext;
            if (![[self.fetchedResultsController fetchedObjects] count]) {
                
                NSString *title = NSLocalizedString(@"NO_DATA_TITLE", @"");
                NSString *text = NSLocalizedString(@"NO_DATA_TEXT", @"");
                [self showAlertWithTitle:title andText:text];
            }
        };
        
        
        // if network service have failed to return countries, initialize with previous database
        // if it succeded drop old database and create new with received data
        // keep reference to DatabaseService it will be used in
        if (error) {
            databaseService = [[DatabaseService alloc] initWithCompletionBlock:completionBlock];
        } else {
            databaseService = [[DatabaseService alloc] initWithCountriesArray:allCountries andCompletionBlock:completionBlock];
        }
    }];
}

- (void)configureSearchBar {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.tableView.tableHeaderView = self.searchController.searchBar;

}

- (void)showAlertWithTitle:(NSString *)title andText:(NSString *)text {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    
    [self.refreshControl endRefreshing];
    self.refreshControl = nil;
    
    if (managedObjectContext) {
        _managedObjectContext = managedObjectContext;
        
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        [self configureSearchBar];
        [self.tableView reloadData];
    } else {
        [self showAlertWithTitle:NSLocalizedString(@"ERROR_TITLE", @"") andText:NSLocalizedString(@"NO_MANAGED_OBJECT_CONTEXT", @"")];
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Country *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        controller.navigationItem.title = object.name;
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell" forIndexPath:indexPath];
    Country *country = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.country = country;

    return cell;
}


#pragma mark - Fetched results controller

- (NSFetchRequest *)countriesFetchRequest {
    NSFetchRequest<Country *> *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[[Country entity] name]];
    
    [fetchRequest setFetchBatchSize:30];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    return fetchRequest;
}

- (NSFetchedResultsController *)fetchResultsCountrollerForSearchText:(NSString *)searchText {
    NSFetchRequest *fetchRequest = [self countriesFetchRequest];
    
    if ([searchText length]) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ OR ANY alternativeSpellings.value CONTAINS[cd] %@", searchText, searchText];
    }
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController<Country *> *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    return [self fetchResultsCountrollerForSearchText:nil];
}

 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}


#pragma mark - Search results updating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchText = searchController.searchBar.text;
    
    [self fetchResultsCountrollerForSearchText:searchText];
    
    [self.tableView reloadData];
}

@end
