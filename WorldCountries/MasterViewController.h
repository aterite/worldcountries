//
//  MasterViewController.h
//  WorldCountries
//
//  Created by armat on 5/24/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "WorldCountries+CoreDataModel.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

