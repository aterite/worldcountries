# README #

### What is this repository for? ###

Sample iOS app for  [https://restcountries.eu](https://restcountries.eu)

flags are from [https://github.com/hjnilsson/country-flags](https://github.com/hjnilsson/country-flags)

* Version 0.1


### How do I get set up? ###

You need Xcode 7+ and Cocoapods installed. Open the project folder and run ***pod install*** in terminal. After that open WorldCountries.xcworkspace

### Dependencies ###
* [SDWebImage](https://github.com/rs/SDWebImage)

dependencies are installed with cocoapods

### DESCRIPTION ###

You can search countries by name or alternative names. 

Because it's a small data, app downloads all countries on each start and saves it locally. Search is also done in locally.

If remote service is not available it uses previously saved local database.

User interface is done for both iPhones and iPads on all orientations. It is done by using UISplitViewController. Constraints in the DetailViewController are slightly modified for iPhones and iPads using size classes.

![Aaland.png](https://bitbucket.org/repo/XX5Xxzq/images/2508257475-Aaland.png)