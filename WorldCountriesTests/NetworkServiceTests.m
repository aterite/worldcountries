//
//  NetworkServiceTests.m
//  WorldCountries
//
//  Created by armat on 5/28/17.
//  Copyright © 2017 metric. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NetworkService.h"

@interface NetworkServiceTests : XCTestCase

@end

@implementation NetworkServiceTests

- (void)downloadData: (void (^)(NSArray<NSDictionary *> * _Nullable allCountries, NSError * _Nullable error))block {
    NSBundle *bundle = [NSBundle bundleForClass:[NetworkServiceTests class]];
    
    NSURL *url = [bundle URLForResource:@"Countries" withExtension:@"json"];
    
    NetworkService *networkService = [[NetworkService alloc] initWithURL:url];
    
    [networkService retrieveAllCountries:block];
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDownloadData {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"get countries"];
    
    __block NSArray *countries;
    
    [self downloadData:^(NSArray<NSDictionary *> * _Nullable allCountries, NSError * _Nullable error) {
        
        countries = allCountries;
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:4.0 handler:nil];
    
    XCTAssertEqual(countries.count, 250, @"Wrong countries number");
    
    NSDictionary *dictionary = countries.firstObject;
    
    XCTAssertTrue([dictionary[@"name"] isEqualToString:@"Afghanistan"]);
}

- (void)testSerializationPerformance {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
        XCTestExpectation *expectation = [self expectationWithDescription:@"get countries"];
        
        [self downloadData:^(NSArray<NSDictionary *> * _Nullable allCountries, NSError * _Nullable error) {
            [expectation fulfill];
        }];
        
        [self waitForExpectationsWithTimeout:4.0 handler:nil];
        
    }];
}

@end
